// -----------------------------------------------------------------------------------------------
/*

Project:    asm56k
Author:     M.Buras (sqward)


*/
// -----------------------------------------------------------------------------------------------

#ifndef _OUTPUT_LOD_H_
#define _OUTPUT_LOD_H_

extern int 	output_format;
extern int	g_output_symbols;

void	SaveFileLod(char* name,char* iname);

#endif //_OUTPUT_LOD_H_