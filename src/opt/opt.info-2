This is opt.info, produced by makeinfo version 4.1 from opt.texi.

   This document describes `opt', v3.19, a subroutine library for
communicating options and parameter values to a C program via the
command line, parameter files, environment variables, or a rudimentary
builtin interactive menu.

   Permission is granted to make and distribute verbatim copies of this
manual provided this permission notice is preserved on all copies.

   Permission is granted to copy and distribute modified versions of
this manual under the conditions for verbatim copying, provided also
that the section entitled "Copying" is included exactly as in the
original, and provided that the entire resulting derived work is
distributed under the terms of a permission notice identical to this
one.

   Permission is granted to copy and distribute translations of this
manual into another language, under the above conditions for modified
versions, except that this permission notice may be stated in a
translation approved by the author.


File: opt.info,  Node: Registering functions (hooks),  Next: Misc,  Prev: Setting some strings,  Up: Programmer Interface

Registering functions (hooks)
-----------------------------

   Hooks are means by which the user can write routines which the `opt'
package calls during its processing.  This enables the programmer to
customize the behavior of `opt' to the program at hand.  Of course
`opt' has to be informed of the function (in the same way that
variables have to be registered, so do functions).  The hook function
should always return an integer, and a value of zero is the default
that tells `opt' that all is okay.  (You can also return `OPT_OK',
which is defined to be zero in `opt.h'.)  The return value tells `opt'
what to do after the hook has been run.  [** Warning: I am thinking of
changing these. -jt **]
`OPT_OK'
     Default.  Keep processing options.

`OPT_ERROR'
     Signals to `opt' that the hook was not successful.  Currently,
     `opt' doesn't do anything differently from what it does if
     `OPT_OK' is returned.

`OPT_EXIT'
     Program exits using the C command `exit(n);' The value of n will
     be zero unless it has been set by the function `optExitNumber(n)'.

`OPT_QUIT'
     Program exits, as with `OPT_EXIT', but it will first check to see
     if a quit-hook has been defined with the `optQuit()' function.  If
     one has, then it will run the quit-hook and then exit.

`OPT_ABORT'
     If you are in the menu, you should be returned to the prompt
     (assuming you configured `opt' to `--enable-longjmp' (default) at
     compile time); but if you are running from the command line, the
     program will quit calling a quit-hook if one has been defined.

   There are basically two kinds of hooks.  The first kind is associated
with a specific variable, and it is run whenever that option is invoked.
These hooks take a single argument, a `void *' that points to the
associated variable.  The second kind of hook is not associated with a
specific variable, and it takes no arguments.  It might be associated
with a command line string (such as `--version' which you might want to
use to make the program write a version number and exit; but see the
`optVersion' function in *Note Setting some strings::.), or with the
general behavior of `opt', such as the quit hook described below.
Here, `OPT_PFI' is declared `typedef int (*OPT_PFI)()' in the header
file `opt.h'; it is a pointer to a function returning an integer.
(Currently we exploit C's loose typecasting rules, so that `OPT_FPI'
refers to a function with any number of arguments, as long as it
returns an integer.  In future versions, we may define `OPT_PFI_V' and
`OPT_PFI_ARG' types, to indicate functions that take arguments `void *'
and `int,char **' respectively.)

   The first kind of hook is registered with the function `opthook()'.
For instance, if you register a hook with a statement such as
     int fix_mon(void *v) { ... }
     int month=9;
     ...
     optreg(&month,OPT_INT,'m',"Month");
     opthook(&month,fix_mon)
   then the function `fix_mon' will be called when the 'm' option is
invoked.  For instance, the function might determine whether the
integer MONTH variable was between 1 and 12, and if not, to fix it in
some way.  Note that the hook function `fix_mon' takes a single `void *'
argument; when `opt' calls the hook function, it will use a pointer to
the variable MONTH as the argument.  You can use the argument to get
(and manipulate) the value of the variable MONTH.  However, if the
variable is global, then you can also manipulate the month directly.
So the following two examples serve the same purpose.
     int fix_mon(void *v)
     {
         /* don't bother using the argument v;
          * just manipulate month directly */
         if (month < 1 || month > 12)
             month=1;
         return OPT_OK;
     }
   and
     int fix_mon(void *v)
     {
         int m;
         /* fix whatever int variable v is pointing to */
         m = *((int *)v);
         if (m < 1 || m > 12)
             m=1;
         *((int *)v) = m;
         return OPT_OK;
     }
   The second example is not as pretty to look at, but it is more
general.  It does not have to know which variable it is fixing, and in
fact you can `opthook' this function to several variables, and it will
apply the `fix_mon' algorithm to those variables as they are invoked.

`void opthook(void *v, OPT_PFI fcn)'
     associates a hook `fcn' with the variable pointed to by V.
     Whenever the option associated with V is invoked, eg from the
     command line, the hook is called.

`void opthook_n(int n, OPT_PFI fcn)'
     associates a hook `fcn' with the N'th option.  N is the value
     returned by the registration function `optreg'.

`int optexec(char *longname, OPT_PFI fcn, char *descript)'
     associates a hook `fcn' to the string pointed to by LONGNAME.  For
     instance, the command `optexec("altversion",print_version,"Write
     version info and exit");' will cause the function
     `print_version()' to be called whenever the string `--altversion'
     appears on the command line.  The function `print_version()' takes
     no argument, and depending on its return value, the program will
     either exit or keep going after printing the version string. (Note
     that `--version' is an opt builtin; see the `optVersion' function
     in *Note Setting some strings::.)

`optMain(OPT_PFI fcn)'
     The most commonly used hook in an `opt' program, at least by me.
     This function is run when the user types `=' at the `opt' menu
     prompt.  Usually, the programmer writes `fcn(argc,argv)' to do
     whatever it is that the program is supposed to do (essentially the
     same as `main(argc,argv)' but without the command line parsing
     that `opt' is taking care of).  The arguments `argc,argv' that
     `fcn' sees are the command line strings that are "leftover" after
     `opt' has finished parsing; `argv[0]' is retained however, so that
     `fcn' behaves just like a `main' function would behave.  When
     `fcn' is finished, you will be returned to the command line
     prompt.  If `opt' was configured at compile time with the
     `--enable-longjmp' (default), then if you interrupt (^C) `fcn'
     while it is running, you will be returned to the command prompt
     (instead of exiting the program completely).

`optRun(OPT_PFI run)'
     This is essentially the same as `optMain', except that the
     function `run()' takes no arguments.  If you set this hook and the
     hook in `optMain', then this hook will be ignored.

`optQuit(OPT_PFI fcn)'
     is a hook that is run just before the program that is using `opt'
     exits.

`optAdditionalUsage(OPT_PFI fcn)'
     This hook is run at the end of the usage message, and can be used
     to provide the user with additional usage information.

`void optExitNumber(int n)'
     If opt has to exit, then call exit(n);


File: opt.info,  Node: Misc,  Prev: Registering functions (hooks),  Up: Programmer Interface

Misc
----

`int optinvoked(void *v)'
     Returns the number of times the option associated with the variable
     pointed to by V was invoked by the user.

`void optPrintUsage()'
     Print the current usage message to standard out.

`void opt_free()'
     If you are worried about memory leaks, you should call this
     routine when you are finished using opt routines.  Most of the
     time, that means you can invoke `opt_free()' right after
     `opt(&argc,&argv)', but after `opt_free()', you won't be able to
     use `optinvoked' for example.  Note that this will not free all
     the memory that `opt' allocated - for instance, it makes copies of
     string parameters that are specified on a command line.  But also
     note that `opt' does not generally take a lot of memory, so if you
     leave off this command, you will nearly always be just fine.


File: opt.info,  Node: Etc,  Prev: Opt,  Up: Top

Etc
***

* Menu:

* Installation::
* recipe::
* Global variables::
* Single file::
* Extensions::
* Bugs::
* Warranty::
* Copying::


File: opt.info,  Node: Installation,  Next: recipe,  Prev: Etc,  Up: Etc

Installation
============

   Because `opt' does not require exotic systems services or esoteric
libraries, installation on a variety of platforms should be
straightforward.  What follows is a guide to installing OPT on a UNIX
system, but I have heard of at least one successful install on a
Microsoft operating system.

   First you have to un-archive the `opt-XXX.tar.gz' file, where `XXX'
is the version number, using commands along these lines:
                gzip -d opt-XXX.tar.gz
                tar xvf opt-XXX.tar
                cd opt-XXX

   Then, it's bascially a generic GNU installation, with `configure',
`make', and `make install'.  More details can be found in the `INSTALL'
file that should be in the distribution, but those are details about
the generic installation procedure, and contain no `opt'-specific notes.

  1. You configure with the command `./configure' where the initial
     `./' is to ensure that you are running the configure script that
     is in the current directory.  You can also use the configure
     command with options.  Type `./configure --help' to see what those
     options are.  One of the most useful is the `--prefix=PATHNAME'
     option, which tells you where to install it.  In this example, the
     file `libopt.a' will be installed in the directory `PATHNAME/lib',
     the header file `opt.h' will be installed in `PATHNAME/include',
     and the info file `opt.info' will be installed in `PATHNAME/info'.
     Some `opt'-specific options are:
    `--disable-longjmp'
          If `longjmp' is enabled (default), then hitting `^C' during a
          run launched from the menu prmopt will return you to the menu
          prompt; if `longjmp' is disabled, then `^C' exits the program
          entirely.  You should only disable longjmp if you have
          trouble compiling.

    `--with-readline'
          If your system has `GNU readline' installed(1), you can
          specify this option to obtain readline features (line editing,
          history recall, etc) in the `opt' menu.  If you will be using
          the menu even a little bit, these features are very
          convenient.

    `--enable-flagonezero'
          By default, flag values of true and false are recorded (to
          the `.opt' file for instance) with `+' or `-', respectively;
          eg, `-v+' turns on the `v'-flag.  But if you invoke
          `--enable-flagonezero', then true and false are encoded as
          `1' and `0' instead.  If you have a lot of long option names,
          it looks a little cleaner (some might argure) to have
          `--verbose=0' rather than the default `--verbose=-'.

  2. The next step is easy, just type `make'.  If this fails, you may
     need to reconfigure in the previous step.  Or, you may have a
     defective version of `opt', or you may be compiling it on a new
     platform

  3. Although it is not necessary, you are encouraged to type `make
     check'.  This runs a number of tests to make sure that the `opt'
     package you just compiled is actually working.  The tests in the
     `test/' directory also serve as example code to show you how to
     use the opt package in your own programs.

  4. The last step is to type `make install' but you should be sure you
     have permission to write to the directories where `libopt.a' and
     `opt.h' will be copied.  This is by default `/usr/local/lib' and
     `/usr/local/include', but that can be changed at the configure
     step (see `INSTALL' for details).  Unless you are installing `opt'
     into your personal directory, you will probably have to become
     "root" before you do this step.

   ---------- Footnotes ----------

   (1) If it doesn't, you can get `readline' from any `GNU' mirror site.


File: opt.info,  Node: recipe,  Next: Global variables,  Prev: Installation,  Up: Etc

Adding `opt' to existing code: a recipe
=======================================

   Suppose your code initially looks like this:
     /* yourprogram.c */
     #include <stdio.h>
     
     int N=5;           /* Global variable */
     
     int
     main(int argc, char **argv)
     {
             /** do some complicated thing  **
              ** that depends on value of N **/
     }

   You want to use the `opt' package to make the variable N into a
parameter that can be altered on the command line.  Here is the recipe.

     /* yourprogramonopt.c */
     #include <stdio.h>
     #include <opt.h>
     
     int N=5;           /* Global variable */
     
     int
     youroldmain(int argc, char **argv)
     {
             /** do some complicated thing  **
              ** that depends on value of N **/
     }
     
     int
     main(int argc, char **argv)
     {
             optreg(&N,OPT_INT,'N',"Number that complicated thing depends on");
             optMain(youroldmain);
             opt(&argc,&argv)
             return youroldmain(argc,argv);
     }
   Basically, you need to include the `opt.h' header file, register the
variables that will be command line parameters, register your original
`main' function (only now renamed not to conflict with the new
`main()'), call the `opt()' function itself, and then call your
original `main()' function (now renamed).

   If you don't want to use a different `main' function, then you don't
have to. In fact the only drawback with this approach is that the user
won't be able to use the `"="' command to run the program from within
the menu. In such a case, you may want to disable the menu anyway,
using the `optDisableMenu()' function.

   If the "complicated thing that depends on N" depends on ARGC,ARGV,
then the ARGC,ARGV that `youroldmain()' will see will be the leftover
arguments on the command line that come after the `-N 5', or the
arguments that come after `--' on the command line.

   When the users use your program in the menu mode, they can try
different values of N, eg
     -> N 5                 ;user types `N 5' in response to prompt `->'
     -> =                   ;user says to do the complicated thing
     5                      ;output of complicated thing, followed by prompt
     -> N 500 =             ;user tries another value, and says run with that
     2 2 5 5 5              ;computer responds
     -> N 12345678          ;user types in too large a value, computer hangs
     ^C                     ;user hits ^C
     ->                     ;computer responds with menu prompt, so user can
                            ;try again with some other value


File: opt.info,  Node: Global variables,  Next: Single file,  Prev: recipe,  Up: Etc

So, you don't like global variables?
====================================

   One, they're not as bad as you might think.  Remember, these are not
obscure variables that will be interacting in odd ways with different
components of a complicated whole; these are the variables that you
want to give the user direct access to.

   Two, although the examples I've given (and the codes I write) use
global variables for the user-alterable parameters, it is possible to
use `opt' without global variables.  I will leave this as an exercise
for the reader who gives a damn.


File: opt.info,  Node: Single file,  Next: Extensions,  Prev: Global variables,  Up: Etc

Single file
===========

   If you want to include `opt' in code that you have written to be
distributed, you are by the GPL quite welcome to do so, following the
usual caveats, provisos, and quid pro quo's.

   You may find it inconvenient, however, to include the full
distribution of opt, with all of its automake'd Makefiles, its multiple
source files, and the extra test and extension directories.  As long as
your distribution is not an extension to opt whose purpose is ever
fancier options parsing, but instead does something useful and just
uses opt for its command line processing, then you are permitted to
include only the minimal source needed to make `libopt.a'.  In fact, to
simplify this task, you can do a `make opt.c' in the `src/' directory
and a single file, called `opt.c' of course, will be generated.  (It is
possible that the `opt' distribution will already have an `opt.c' made
for you.) You are free to include `opt.c' along with `opt.h' in your
distribution.  You don't even need to make a `libopt.a' if you don't
want to.  Just have your `Makefile' include lines to this effect:
             opt.o: opt.c opt.h
                    $(CC) $(CFLAGS) -c opt.c
     
             yourprogram.o: yourprogram.c opt.h
                    ...
     
             yourprogram: yourpgram.o ... opt.o
                    $(CC) $(LDFLAGS) -o yourprogram yourprogram.o ... opt.o
   Note that when you make `opt.c', a pair of GPL-style copyright-like
paragraphs will appear.  (They are not actually copyright statements,
but that's a long story: see Copying.)  You are required to keep those
paragraphs intact, and make sure it includes a pointer so that users of
your distribution can find my full `opt' distribution, if they want it.


File: opt.info,  Node: Extensions,  Next: Bugs,  Prev: Single file,  Up: Etc

Extensions
==========

`tkopt'
     is a Tk/Tcl script that is used as a front end to programs that
     have an `opt' interface.  To use it type
                  tkopt program [options]
     and a window will pop up which will permit you to edit all the
     registered options and parameters, and to run the program.  The
     beauty of `tkopt' is that it knows nothing about the program
     `program' beforehand.  It learns what the options are by running
     `program --help' and parsing the standard usage message that is
     output.  It also runs `program %tmp.opt' and then reads the
     `tmp.opt' file to determine the defaults.

`opt.pl'
     is an old Perl options processing library that is no longer
     supported, and no longer included with distributions of opt, but
     in it's place...

`Opt.pm'
     is a Perl package module which can be `use''d by other Perl
     scripts to achieve an opt-like interface.  The main bug is that
     you can already get pretty good option parsing with only a few
     lines of Perl, and there exist other packages (eg, `Getopt::Long')
     that perform quite powerful processing.  What `Opt.pm' provides is
     nearly identical behavior to the C version.  Thus, you can use
     `tkopt' as a front-end to perl scripts using `Opt.pm' just like
     you can use it for C programs linked with `libopt.a'.

`READLINE'
     If you configure `opt' with the `--with-readline' feature, then
     `GNU readline' features (line editing, previous line retrieval,
     etc) will be available in the menu mode of `opt'.  To link your
     program to this code, you'll need to have the `readline' and
     `termcap' libraries available. But this is all done automagically
     (where by "magic" I mean code that is embarassingly complicated)
     by the configure script.  So if `libopt.a' is built with this
     option enabled, it will actually incorporate the `readline' and
     `termcap' libraries within itself, and you don't need to do
     anything different on the linking step.  In particular, you do NOT
     need to add `-lreadline -ltermcap' to the `cc' command line.  If
     it fails, and you don't feel like trying to figure out why, just
     reconfigure without the `--with-readline' option.  It's nice, but
     not really crucial.


File: opt.info,  Node: Bugs,  Next: Warranty,  Prev: Extensions,  Up: Etc

Bugs
====

   Using `opt' promotes the use of global variables for the parameters
that `opt' sets.  Global variables are generally considered harmful to
your health, but in my experience, this has rarely been a problem for
variables that you want the user to have access to anyway.  I have seen
various convoluted schemes for getting around this, but I have not been
convinced of their usefulness.

   Another bug is that `opt' doesn't look much like the standard and
GNU's `getopt' package, even though it does pretty much the same thing.
Partly this is a design choice; I wanted something that was very easy
to "attach" to the code.  In particular, with `opt', you register
options and associate them with variables; this tends to be a little
more compact (and in my view more convenient) than the loop and
case-statement approach used by `getopt'.  Also, `opt' has a few more
bells and whistles.  If I were smart, I would have built `opt' as an
add-on to the standard `getopt'.


File: opt.info,  Node: Warranty,  Next: Copying,  Prev: Bugs,  Up: Etc

Warranty
========

   none.


File: opt.info,  Node: Copying,  Prev: Warranty,  Up: Etc

Copying
=======

   The subroutines and source code in the `opt' package are free.

   However, I was paid to write this software.  That is to say, I wrote
this software while being paid by the University of California to work
for the Department of Energy at Los Alamos National Laboratory.  I like
my job, and am grateful to the Laboratory for the opportunity to do
interesting work and collect a nice paycheck.  However, I do NOT own the
copyright on this software, and the conditions for redistributing `opt'
reflect that.

   These conditions, encrypted in legalese, are described in the file
`COPYING' that should be included in the `opt' distribution.  For
practical purposes, this is the same as the GNU General Public License,
although I read it as saying that the US Government paid for this
software, and the US Government doesn't consider itself bound by the
more restrictive aspects of the GPL.  But the rest of you are.

   My own imprecise interpretation of the GPL, as it applies to `opt',
follows.  I should say that this is a heavily edited version of a
`Copying' section that I copied from some other GNU package (now
forgotten).

   Everyone is free to use this software and free to redistribute it on
a free basis.  The `opt' library is not in the public domain; there are
restrictions on its distribution, but these restrictions are designed
to permit everything that a good cooperating citizen would want to do.
What is not allowed is to prevent or inhibit others from further
sharing any version of this software that they might get from you.

   Specifically, I want to make sure that you have the right to give
away copies of `opt', that you receive source code or else can get it
if you want it, that you can change `opt' or use pieces of it in new
free programs, and that you know you can do these things.

   To make sure that everyone has such rights, I cannot and do not give
you the "right" to deprive anyone else of these rights.  For example, if
you distribute copies of the `opt'-related code, you must give the
recipients all the rights that you have.  You must make sure that they,
too, receive or can get the source code.  And you must tell them their
rights.

   Also, for my own protection, I must make certain that everyone finds
out that there is no warranty for `opt'.  If this software is modified
by someone else and passed on, I want the recipients to know that what
they have is not what I distributed, so that any problems introduced by
others will not reflect on my reputation, feeble though it may be.

   Let me say that by `opt'-related, I mostly mean `opt'-derived.  If
your software does something substantially different from `opt', but
uses `opt' for its command line processing, then you can do what pretty
much you like with that code: sell it for a profit, design weapons of
mass destruction, etc.  That's my own view.  I should note that a more
common interpretation of the GPL holds that if you use a GPL'd library
in your code, then your code must be GPL'd.  Just because I don't hold
this view doesn't mean you are off the hook; it just means that I am
unlikely to sue you if you use my package under this less restrictive
interpretation.

   But in any case, you should make it clear to the users of your code
that the options parsing is done by software that is free; you should
tell them that free software is a wonderful concept; and you should
provide the `opt' source code, or at least provide the users with a
pointer to where the code is available.  (Currently, that is
`http://nis-www.lanl.gov/~jt/Software'.)

   Happy hacking...


