// -----------------------------------------------------------------------------------------------
/*

Project:    asm56k
Author:     M.Buras (sqward)


*/
// -----------------------------------------------------------------------------------------------
#ifndef _ASM_TYPES_H_
#define _ASM_TYPES_H_

typedef unsigned int uint;
typedef unsigned char uchar;

typedef uint bool;
typedef unsigned long long u64;
typedef long long s64;
typedef unsigned long u32;
typedef long s32;
typedef unsigned char u8;
typedef char s8;

#define true 1
#define false 0

#endif //_ASM_TYPES_H_